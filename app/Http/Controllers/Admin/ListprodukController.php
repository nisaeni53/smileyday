<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Produk;
use Illuminate\Http\Request;

class ListprodukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['listproduk'] = Produk::all();
        return view('admin.listproduk.listproduk', $data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.listproduk.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        dd($request->all());
        $rule = [
            'nama_produk' => 'required',
            'deskripsi_produk' => 'required|string',
            'foto_produk' => 'required',
        ];

        $this->validate($request, $rule);
        $foto='kosong jpg';
        $request['foto_produk']=$foto;
        $input = $request->all();
        $status = Produk::create($input);
        if ($status){
            return redirect('listproduk')->with('success', 'Data berhasil ditambahkan');
        }else{
            return redirect('listproduk/create')->with('error', 'Data gagal ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rule = [
            'nama_produk' => 'required',
            'keterangan' => 'required|string',
        ];

        $this->validate($request, $rule);
        $input = $request->all();
        $produk = produk::find($id);
        $status = $produk->update($input);
        if ($status){
            return redirect('/admin/listproduk/listproduk')->with('success', 'Data berhasil diubah');
        }else{
            return redirect('/admin/listproduk/form')->with('error', 'Data gagal diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $produk = Produk::find($id);
        $status = $produk->delete();
        if ($status){
            return redirect('/admin/listproduk/listproduk')->with('success', 'Data berhasil di delete');
        }else{
            return redirect('/admin/listproduk/form')->with('error', 'Data gagal di delete');
        }
    }
}
