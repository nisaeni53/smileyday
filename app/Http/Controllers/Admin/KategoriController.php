<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['kategori'] = Kategori::all();
        return view('admin.kategori.index', $data);
        //$data_kategori['kategori'] = Kategori::orderBy('id', 'asc')->get();
        //return view('admin.kategori.index', $data_kategori);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.kategori.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rule = [
            'jenis' => 'required',
            'keterangan' => 'required|string',
        ];

        $this->validate($request, $rule);
        $input = $request->all();
        $status = Kategori::create($input);
        if ($status){
            return redirect('kategori')->with('success', 'Data berhasil ditambahkan');
        }else{
            return redirect('kategori/create')->with('error', 'Data gagal ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $kategori = Kategori::find($id);
        $data['kategori'] = $kategori;
        return view('admin.kategori.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rule = [
            'jenis' => 'required',
            'keterangan' => 'required|string',
        ];

        $this->validate($request, $rule);
        $input = $request->all();
        $kategori = Kategori::find($id);
        $kategori->jenis = $request->jenis;
        $kategori->keterangan = $request->keterangan;


        $status = $kategori->save();
        if ($status){
            return redirect('kategori')->with('success', 'Data berhasil diubah');
        }else{
            return redirect('kategori/form')->with('error', 'Data gagal diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $kategori = Kategori::find($id);
        $status = $kategori->delete();
        if ($status){
            return redirect('kategori')->with('success', 'Data berhasil di hapus');
        }
    }
}
