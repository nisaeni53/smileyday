<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Warna;
use Illuminate\Http\Request;

class WarnaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['warna'] = Warna::all();
        return view('admin.warna.warna', $data);
        //$data_warna['warna'] = Warna::orderBy('id', 'asc')->get();
        //return view('admin.warna.warna', $data_warna);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.warna.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rule = [
            'warna' => 'required',
            'keterangan' => 'required|string',
        ];

        $this->validate($request, $rule);
        $input = $request->all();
        $status = Warna::create($input);
        if ($status){
            return redirect('warna')->with('success', 'Data berhasil ditambahkan');
        }else{
            return redirect('warna/create')->with('error', 'Data gagal ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $warna = Warna::find($id);
        $data['warna'] = $warna;
        return view('admin.warna.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rule = [
            'warna' => 'required',
            'keterangan' => 'required|string',
        ];

        $this->validate($request, $rule);
        $input = $request->all();
        $warna = Warna::find($id);
        $warna->warna = $request->warna;
        $warna->keterangan = $request->keterangan;


        $status = $warna->save();
        if ($status){
            return redirect('warna')->with('success', 'Data berhasil diubah');
        }else{
            return redirect('warna/form')->with('error', 'Data gagal diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $warna = Warna::find($id);
        $status = $warna->delete();
        if ($status){
            return redirect('warna')->with('success', 'Data berhasil di hapus');
        }
    }
}
