<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    use HasFactory;

    protected $table = 't_transaksi';
    protected $fillable = [
        'id_kasir', 'id_pembeli', 'tanggal', 'harga_total'
    ];

    public function kasir(){
        return $this->hasOne(User::class, 'id' ,'id_kasir');
    }

    public function pembeli(){
    return $this->hasOne(User::class, 'id' ,'id_pembeli');
    }

    public function transaksi_detail(){
        return $this->hasMany(DetailTransaksi::class, 'id_transaksi' ,'id');
    }

}
