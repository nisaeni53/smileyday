<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailProduk extends Model
{
    use HasFactory;

    protected $table = 't_detail_produk';
    protected $fillable = [
        'id_produk', 'id_warna', 'stok'
    ];

    public function produk(){
        return $this->hasOne(Produk::class, 'id' ,'id_produk');
    }

    public function warna(){
        return $this->hasOne(Warna::class, 'id' ,'id_warna');
    }

    public function transaksi_detail(){
        return $this->hasMany(DetailTransaksi::class, 'id_detail_produk' ,'id');
    }

}
