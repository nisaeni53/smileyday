<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;

    protected $table = 't_produk';
    protected $fillable = [
        'id_kategori', 'nama_produk', 'hpp', 'harga', 'deskripsi_produk', 'foto_produk'
    ];

    public function kategori(){
        return $this->hasOne(Kategori::class, 'id' ,'id_kategori');
    }

    public function produk_detail(){
        return $this->hasOne(DetailProduk::class, 'id' ,'id_detail_produk');
    }



}

