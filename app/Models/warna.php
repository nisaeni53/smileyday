<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Warna extends Model
{
    use HasFactory;

    protected $table = 't_warna';
    protected $fillable = [
        'warna', 'keterangan'
    ];

    public function produk_detail(){
        return $this->hasMany(DetailProduk::class, 'id_warna' ,'id');
    }
}
