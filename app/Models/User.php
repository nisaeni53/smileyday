<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $table = 't_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'role',
        'alamat',
        'email',
        'password',
        'no_telfon'
    ];

    public function kasir(){
        return $this->hasMany(Transaksi::class, 'id_kasir' ,'id');
    }

    public function pembeli(){
    return $this->hasMany(Transaksi::class, 'id_pembeli' ,'id');
    }
    
}
