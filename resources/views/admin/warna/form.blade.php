@extends('admin.layout.app')

@section('content')

<!-- BEGIN: Content-->
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Form Warna</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Forms</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="#">Form Layouts</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="app-todo.html"><i class="mr-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="mr-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="mr-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="mr-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">

            <!-- Basic Vertical form layout section start -->
            <section id="basic-vertical-layouts">
                <div class="row">
                    <div class="col-md-12 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{{@$warna ? 'Ubah' : 'Tambah'}} Warna</h4>
                            </div>
                            <div class="card-body">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" role="alert">
                                        <h4 class="alert-heading">Error!</h4>
                                        @foreach ($errors->all() as $error)
                                            <div class="alert-body">
                                            {{$error}}
                                            </div>
                                        @endforeach
                                        </div>
                                    @endif
                                <form class="form form-vertical" method="POST" action="{{@$warna ? route('warna.update', $warna->id) : route('warna.store')}}" enctype="multipart/form-data">
                                    @csrf
                                    @if(@$warna)
                                        {{method_field('patch')}}
                                    @endif
                                    <div class="row">
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="email-id-icon">Warna</label>
                                                <div class="input-group input-group-merge">
                                                    <input type="text" id="warna" class="form-control"
                                                       name="warna" placeholder="Jenis"
                                                        value="{{old('warna', @$warna ? $warna->warna : '') }} "/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="password-icon">Keterangan</label>
                                                <div class="input-group input-group-merge">
                                                    <textarea name="keterangan" id="keterangan" cols="30"
                                                    rows="10" class="form-control">{{old('keterangan', @$warna ? $warna->keterangan : '')}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <button type="reset" class="btn btn-outline-secondary">Back</button>
                                            <button type="submit" class="btn btn-primary mr-1">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Basic Vertical form layout section end -->
        </div>
    </div>
</div>
<!-- END: Content-->

@endsection
