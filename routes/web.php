<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\KategoriController;
use App\Http\Controllers\Admin\WarnaController;
use App\Http\Controllers\Admin\ListprodukController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin/dasboard/', [DashboardController::class, 'index'])->name('dashboard');

Route::resource('listproduk', ListprodukController::class);

Route::resource('kategori', KategoriController::class);

Route::resource('warna', WarnaController::class);

//Route::get('/admin/kategori/index', [KategoriController::class, 'index'])->name('kategori');
//Route::get('/admin/kategori/form', [KategoriController::class, 'create'])->name('form');
//Route::post('/admin/kategori/index', [KategoriController::class, 'store'])->name('formStore');
//Route::patch('/admin/kategori/index/{id}', [KategoriController::class, 'update'])->name('formUpdate');
//Route::delete('/admin/kategori/index/{id}', [KategoriController::class, 'destroy'])->name('formDelete');


//Route::get('/admin/warna/warna', [WarnaController::class, 'index'])->name('warna');
//Route::get('/admin/warna/form', [WarnaController::class, 'create'])->name('formWarna');

//Route::get('/admin/listproduk/listproduk', [ListprodukController::class, 'index'])->name('listproduk');
//Route::get('/admin/listproduk/form', [ListprodukController::class, 'create'])->name('formListproduk');
